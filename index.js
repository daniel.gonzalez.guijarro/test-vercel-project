const express = require('express')
const path = require('path')
const cors = require('cors');

const userRouter = require('./routes/user.routes');
const accountRouter = require('./routes/account.routes')
const amountRouter = require('./routes/amount.routes')

require('dotenv').config();

const db = require('./db.js')

db.connect()

require('./passport');

const PORT = process.env.PORT || 5000;

const server = express();

server.use((req, res, next) => {
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    res.header('Access-Control-Allow-Credentials', true)
    res.header('Access-Control-Allow-Headers', 'Content-Type')
    res.header('Content-Type", "text/html"')
    next()
});

server.use(cors({
    origin: ['*'],
    credentials: true,
    methods: ['GET', 'PUT', 'POST', 'DELETE'],
    allowedHeaders: ['Content-Type', 'Authorization'],
}))

server.use(express.json())
server.use(express.urlencoded({ extended: true }))

server.use(express.static(path.join(__dirname, 'public')))

server.use("/", userRouter)
server.use("/account", accountRouter)
server.use("/amount", amountRouter)

server.use('*', (req, res, next) => {
    const error = new Error('Route not foundddf')
    error.status = 404
    next(error)
})

server.use((err, req, res, next) => {
    console.log(err);
    return res.status(err.status || 500).json(err).json("error in the route")
})

server.listen(PORT, () => {
    console.log(`Server started on http://localhost:${PORT}`)
});
