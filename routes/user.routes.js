const express = require('express')
const usersController = require('../controller/user.controller')

const router = express.Router()

router.get('/', (req, res) => {
    return res.send("ok")
})
router.post('/register', usersController.registerPost)
router.post('/get', usersController.getUser)
router.post('/login', usersController.loginPost)
router.get('/logout', usersController.logoutPost)
router.post('/save', usersController.saveUser)

module.exports = router