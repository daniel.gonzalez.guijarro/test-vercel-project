const express = require('express')
const router = express.Router()

const accountController = require('../controller/account.controller')

router.post('/create', accountController.create)
router.delete('/delete', accountController.delete)
router.post('/edit', accountController.edit)
router.post('/get', accountController.get)
router.post('/share', accountController.share)

module.exports = router