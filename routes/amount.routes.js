const express = require('express')
const amountController = require('../controller/amount.controller')

const router = express.Router()

router.post('/insert', amountController.insert)
router.delete('/delete', amountController.delete)
router.post('/edit', amountController.edit)
router.post('/get', amountController.get)

module.exports = router;