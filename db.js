// const { MongoClient } = require('mongodb')
// const DB_URL = process.env.MONGODB_URI;

// const client = new MongoClient(DB_URL);

// const connect = async (req, res, next) => {

//     await client.connect();

//     try {
//         // Connect to the MongoDB cluster
//         await client.connect();

//         // Make the appropriate DB calls
//         await listDatabases(client);

//     } catch (e) {
//         console.error(e);
//     } finally {
//         await client.close();
//     }
// }

// async function listDatabases(client) {
//     databasesList = await client.db().admin().listDatabases();

//     console.log("Databases:");
//     databasesList.databases.forEach(db => console.log(` - ${db.name}`));
// };

// module.exports = { DB_URL, connect }

const mongoose = require('mongoose')

require('dotenv').config()

const DB_URL = process.env.MONGODB_URI

console.log('DB_URL:', DB_URL)

const connect = () => {

    mongoose
        .connect(DB_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        })
        .then(() => {
            console.log("Success connecteded to DB")
        })
        .catch((error) => {
            console.log("Error connecting to DB: ", error)
        })
}

module.exports = { DB_URL, connect };
