const { default: mongoose } = require('mongoose')
const Account = require('../models/Account')
const { findById } = require('../models/Amount')
const User = require('../models/User')

module.exports = {
    create: async (req, res) => {
        console.log("creating account", req.body)
        const { userId, name } = req.body
        const newAccount = new Account({
            users: req.body.userId,
            name
        })
        const response = await newAccount.save()
        if (response) {
            const resp = await User.findByIdAndUpdate(userId,
                { $push: { accounts: response._id } },
                { new: true }
            )
            return res.status(201).json(
                {
                    message: "cuenta creada correctamente",
                    response
                })
        }
        return res.status(400).json("No se ha podido crear la cuenta")
    },

    delete: (req, res) => {
        res.status(200).json("delete")

    },

    edit: (req, res) => {
        res.status(200).json("edit")
    },

    get: async (req, res) => {
        const response = await User.findById(req.body.userId)
            .populate({
                path: 'accounts',
                populate: {
                    path: 'amounts',
                    model: 'amounts',
                    options: { sort: { date: 1 } }
                }
            })
        if (response) {
            return res.status(200).json(response)
        }
        return res.status(400).json("No se encuentra la cuenta")
    },

    share: async (req, res) => {
        const { accountId, userId, email } = req.body
        const response = await User.updateMany({ email: email },
            {
                'share.account': accountId,
                'share.from': userId
            })
        if (response.modifiedCount !== 0) {
            return res.status(200).json("Petición enviada")
        }
        return res.status(404).json("No se encuentra usuario")
    }
}