const Account = require('../models/Account')
const Amount = require('../models/Amount')

module.exports = {
  insert: async (req, res, next) => {
    console.log("amount", req.body)
    const { type, userId, accountId, amount, description, date, spend } = req.body
    const newAmount = new Amount(
      {
        type,
        user: userId,
        account: accountId,
        spend,
        amount,
        description,
        date
      })

    const response = await newAmount.save()
    if (response) {
      const resp = await Account.findByIdAndUpdate(accountId,
        { $push: { amounts: response._id } },
        { new: true }
      )
      if (resp) {
        return res.status(201).json(newAmount)
      }
    }
    return res.status(404).json("No se puede añadir el registro")
  },

  delete: async (req, res, next) => {
    const response = await Amount.findByIdAndDelete(req.body.amountId)
    if (response) {
      const resp = await Account.findByIdAndUpdate(req.body.accountId,
        { $pull: { amounts: req.body.amountId } },
      )
      return res.status(200).json("Registro borrado")
    }
    return res.status(400).json("No se encuentra el registro solicitado")

  },

  edit: async (req, res, next) => {
    console.log(req.body)
    const { amountId, amount, type, date, description, spend } = req.body
    const response = await Amount.findByIdAndUpdate(amountId,
      {
        amount,
        type,
        date,
        spend,
        description
      },
      { new: true }
    )
    if (response) {
      return res.status(200).json(response)
    }
  },
  get: async (req, res, next) => {
    const response = await Account.findById(req.body.accountId).
      populate('amounts')
    if (response) {

      return res.status(200).json(response)
    }
    return res.status(404).json("Cuenta no encontrada")
  }

}