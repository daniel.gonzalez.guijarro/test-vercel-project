const User = require('../models/User')
const bcrypt = require("bcrypt")
require('dotenv').config()

const saltRound = process.env.SALT

module.exports = {

  saveUser: async (req, res) => {
    console.log(req.body)
    const { name, cp, surname, city, email, address } = req.body
    try {
      const user = await User.findOneAndUpdate(
        { email },
        { name, cp, surname, city, address },
        { new: true }
      )
      console.log(user)
      if (user) {
        return res.status(200).json(user)
      }
      return res.status(400)

    } catch (error) {
      console.log("eeror", error)
      return res.status(404).json(error)
    }
  },
  getUser: async (req, res) => {
    try {
      const user = await User.findById(req.body.userId)
      console.log("find user getUser", user)
      if (user) {
        return res.status(200).json(user)
      } else {
        return res.status(404).json(err)
      }
    } catch (err) {
      console.log("ha habido un error", err)
      return res.status(404).json(err)
    }
  },
  registerPost: async (req, res) => {
    console.log("req login", req.body)
    const { email, name, password } = req.body
    try {
      const previousUser = await User.findOne({
        email: email.toLowerCase(),
      })
      console.log(previousUser)
      if (previousUser) {
        const error = new Error("The user already exists")
      }
      const hash = await bcrypt.hash(password, Number(saltRound))
      console.log('Llega petición', hash);
      const newUser = new User({
        name: name ? name : 'none',
        email: email.toLowerCase(),
        password: hash,
      })
      const savedUser = await newUser.save()
      console.log("USER CONTROLLER", savedUser)
      return res.status(200).json(savedUser)
    } catch (err) {
      console.log("ha habido un error", err)
      return res.status(404).json(err)
    }
  },

  loginPost: async (req, res) => {
    console.log("req login", req.body)
    try {
      const currentUser = await User.findOne({ email: req.body.email.toLowerCase() })
        .populate({
          path: 'accounts',
          populate: {
            path: 'amounts',
            model: 'amounts',
            options: {
              sort: { 'createdAt': -1 }
            }
          }
        })
      console.log("PASSPORT currentuser", currentUser)
      if (!currentUser) {
        const error = new Error("The user does not exist!");
        return res.status(404).json(error)

      }
      const isValidPassword = await bcrypt.compare(
        req.body.password,
        currentUser.password
      )
      if (!isValidPassword) {
        const error = new Error("The email or password is invalid!")
        return res.status(404).json(error)
      }
      return res.status(200).json(currentUser)
    } catch (error) {
      console.log("ha habido un error", error)
      return res.status(404).json(error)
    }
  },
  logoutPost: (req, res, next) => {
    if (req.user) {
      return res.status(200).json({ message: 'Logout successful' })
    }
    else {
      return res.status(401).json({ message: 'Unexpected error' })
    }
  },
};