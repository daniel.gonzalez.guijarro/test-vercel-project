const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema(
    {
        email: {
            type: String,
            unique: true,
            required: true,
        },
        share: {
            from: {
                type: mongoose.Types.ObjectId
            },
            account: {
                type: mongoose.Types.ObjectId
            }
        },
        password: { type: String, required: true },
        name: { type: String },
        surname: { type: String },
        city: { type: String },
        cp: { type: String },
        address: { type: String },
        accounts: [{
            type: mongoose.Types.ObjectId,
            ref: 'accounts'
        }],

    },
    {
        timestamps: true,
    }
);

const User = mongoose.model("Users", userSchema);

module.exports = User;
