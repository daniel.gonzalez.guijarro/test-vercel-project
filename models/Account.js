const mongoose = require('mongoose')
const Schema = mongoose.Schema

const accountSchema = new Schema(
    {
        name: { type: String, required: true },
        types: [
            {
                type: String,
                enum: {
                    values: ['sumar', 'restar']
                }
            }
        ],
        users: [
            {
                type: mongoose.Types.ObjectId,
                ref: 'users'
            }
        ],
        amounts: [
            {
                type: mongoose.Types.ObjectId,
                ref: 'amounts',
                autopopulate: true
            }
        ]

    },
    {
        timestamps: true
    }
)

const Account = mongoose.model('accounts', accountSchema)

module.exports = Account