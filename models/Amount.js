const mongoose = require('mongoose')
const Schema = mongoose.Schema

const amountSchema = new Schema(
    {
        type: { type: String, required: true },
        amount: { type: Number, required: true },
        description: { type: String },
        spend: { type: Boolean },
        date: {
            type: Date,
            required: true
        },
        user: {
            type: mongoose.Types.ObjectId,
            ref: 'users',
            required: true
        },
        account: {
            type: mongoose.Types.ObjectId,
            ref: 'accounts',
            required: true
        }
    },
    {
        timestamps: true
    }
)

const Amount = mongoose.model('amounts', amountSchema)

module.exports = Amount